<?php
namespace App\Controller;

use App\Exception\RouteException;

/**
 * Class DefaultController
 * @package App\Controller
 */
class DefaultController
{
    /**
     * @throws RouteException
     */
    public function actionError()
    {
        throw new RouteException();
    }
}
