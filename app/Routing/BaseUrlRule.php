<?php
namespace App\Routing;

/**
 * Class BaseUrlRule
 * @package App\Routing
 */
abstract class BaseUrlRule
{
    abstract public function parseUrl($url, $pathInfo);
    abstract public function createUrl($route, $params);
}
