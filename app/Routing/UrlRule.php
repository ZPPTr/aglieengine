<?php


namespace App\Routing;


class UrlRule extends BaseUrlRule
{
    public $route;
    public $pattern;
    public $urlSuffix = '';
    public $caseSensitive = false;
    public $defaultParams = array();
    public $routePattern;

    public function __construct($route, $pattern) {
        if (is_array($route))  {
            foreach (array('urlSuffix', 'caseSensitive', 'defaultParams') as $name) {
                if (isset($route[$name])) {
                    $this->$name=$route[$name];
                }
            }
            if (isset($route['pattern'])) {
                $pattern = $route['pattern'];
            }
            $route = $route[0];
        }
        $this->route = trim($route,'/');
        $this->pattern = $pattern;
    }

    /**
     * @param $url
     * @param $pathInfo
     */
    public function parseUrl($url, $pathInfo)
    {
        return $url;
    }

    public function createUrl($route, $params)
    {
        // TODO: Implement createUrl() method.
    }
}
