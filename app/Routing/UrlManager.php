<?php
namespace App\Routing;

/**
 * Class UrlManager
 * @package App\Routing
 */
class UrlManager
{
    /**
     * @var BaseUrlRule[]|array[]
     */
    public $rules = [];

    /**
     * Adds rules into the list
     * @param array $rules
     */
    public function addRules(array $rules) {
        foreach($rules as $pattern => $route) {
            $this->rules[] = $this->createUrlRule($route, $pattern);
        }
    }

    /**
     * @param string $url
     * @param $pathInfo
     * @return false
     */
    public function parseUrl($url, $pathInfo) {
        foreach ($this->rules as $i=>$rule) {
            if (is_array($rule)) {
                $rule = $this->createRuleObject($rule);

                $this->rules[$i] = $rule;
            }
            $route = $rule->parseUrl($url, $pathInfo);
            if ($route !== false) {
                return $route;
            }
        }
        return false;
    }

    /**
     * @param string $route
     * @param array $params
     * @return string
     */
    public function createUrl($route, $params = array()) {
        foreach ($this->rules as $rule) {
            $url = $rule->createUrl($route, $params);
            if ($url !== false) {
                return '/' . $url;
            }
        }
        return $route . '?' . $this->createPathInfo($params);
    }

    /**
     * @param array $params
     * @return string
     */
    public function createPathInfo($params = []) {
        $items = [];
        foreach ($params as $key=>$value) {
            $items[] = urlencode($key) . '=' . urlencode($value);
        }
        return implode('&', $items);
    }

    /**
     * @param string $route
     * @param string $pattern
     * @return UrlRule|array|string
     */
    private function createUrlRule($route, $pattern) {
        if (is_array($route) && isset($route['class'])) {
            return $route;
        } else {
            return new UrlRule($route, $pattern);
        }
    }

    /**
     * @param array $options
     * @return mixed
     */
    private function createRuleObject($options) {
        $class = $options['class'];
        unset($options['class']);
        $rule = new $class();
        foreach ($options as $key=>$value) {
            $rule->$key = $value;
        }
        return $rule;
    }
}
