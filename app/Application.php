<?php
namespace App;

use App\Routing\UrlManager;

/**
 * Class Application
 * @package App\Bootstrap
 */
class Application
{
    public $urlManager;

    /**
     * Application constructor.
     */
    private function __construct() {
        $this->urlManager = new UrlManager();
    }

    /** @var self $_instance */
    private static $_instance;

    /**
     * @return Application
     */
    public static function app() {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function run() {

        // Add rules
        $rules = require_once(__DIR__ . '/config/routes.php');
        $this->urlManager->addRules($rules);

        // Get route based on current url
        $route = $this->urlManager->parseUrl($_SERVER['REQUEST_URI'], $_SERVER['PATH_INFO']);


        if (!$route) {
            $route = 'default/error';
        }
        print_r($route.PHP_EOL);

        // explode the route
        $route = explode('/', $route);


        $controllerClass = ucfirst($route[1]) . 'Controller';
        require_once(__DIR__ . '/Controller/' . $controllerClass . '.php');
        $controller = new $controllerClass();
        $actionMethod = 'action' . ucfirst($route[2]);
        $controller->$actionMethod();
    }
}
