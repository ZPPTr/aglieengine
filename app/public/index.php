<?php
use App\Application;

require_once (__DIR__ . '/../Application.php');
require_once (__DIR__ . '/../Routing/UrlManager.php');
require_once (__DIR__ . '/../Routing/BaseUrlRule.php');
require_once (__DIR__ . '/../Routing/UrlRule.php');
require_once (__DIR__ . '/../Controller/MediaController.php');
require_once (__DIR__ . '/../Controller/DefaultController.php');

Application::app()->run();
